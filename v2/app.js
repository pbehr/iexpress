
/*************************************************************
 *  Test application app.js Version 2
 *************************************************************/

const express = require('express'),
      api_V2 = express.Router();

let   dbPool = {},
      debug = false;



api_V2.use( function(req, res, next) {
	// The database connection pool is sent in through 
	// app.locals (available here as req.app.locals).
	dbPool = req.app.locals.dbPool;
	debug = req.app.locals.debug;
	next();
});


api_V2.post('/:route' , function(req, res){
	
	if (debug) {
        console.log('POST /' + req.params.route);
    }
	
	let sqlString = "SELECT LSTNAM FROM QIWS.QCUSTCDT LIMIT 2";
	
	dbPool.runStmt( sqlString, (sqlResult) => {
		let apiResult = {
				"success" : true,
				"message" : "You are using the most current version",
				"records" : sqlResult.length,
				"data" : sqlResult
		}
		res.status(200).json(apiResult);
		return;
	});
});



api_V2.get('/:route' , function(req, res){
	
	if (debug) {
        console.log('GET /' + req.params.route);
    }
	
	let sqlString = "SELECT LSTNAM FROM QIWS.QCUSTCDT LIMIT 2";
	
	dbPool.runStmt( sqlString, (sqlResult) => {
		let apiResult = {
				"success" : true,
				"message" : "You are using the most current version",
				"records" : sqlResult.length,
				"data" : sqlResult
		}
		res.status(200).json(apiResult);
		return;
	});
});

module.exports = api_V2;
