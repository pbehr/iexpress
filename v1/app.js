
/*************************************************************
 *  Test application app.js Version 1
 *************************************************************/

const express = require('express'),
      api = express.Router();


api.post('/:route' , function(req, res){

	if (debug) {
        console.log('POST /' + req.params.route);
    }

	let sqlString = "SELECT LSTNAM FROM QIWS.QCUSTCDT LIMIT 2";

	dbPool.runStmt( sqlString, (sqlResult) => {
		let apiResult = {
				"success" : true,
				"message" : "You should upgrade to API Ver.2",
				"records" : sqlResult.length,
				"data" : sqlResult
		}
		res.status(200).json(apiResult);
		return;
	});
});



api.get('/:route' , function(req, res){

	if (debug) {
        console.log('GET /' + req.params.route);
    }

	let sqlString = "SELECT LSTNAM FROM QIWS.QCUSTCDT LIMIT 2";

	dbPool.runStmt( sqlString, (sqlResult) => {
		let apiResult = {
				"success" : true,
				"message" : "You should upgrade to API Ver.2",
				"records" : sqlResult.length,
				"data" : sqlResult
		}
		res.status(200).json(apiResult);
		return;
	});
});

module.exports = api;
