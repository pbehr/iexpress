const // Express is a fast, unopinionated, minimalist web framework for Node.js
      express = require('express'),

      // Helmet *helps* with security by setting various HTTP headers
      helmet = require('helmet'),

      // The path module provides utilities for working with file and directory paths.
      path = require('path'),

      // This is the entirety of what's necessary in order to do primitive connection pooling.
      // https://www.mcpressonline.com/analytics-cognitive/db2/techtip-node-js-db2-connection-pool
      db = require('./util/db2ipool'),

      // JSON API is encapsulated and versioned
      apiVersion1 = require('./v1/app'),
      apiVersion2 = require('./v2/app');


const app = express(),
      port = process.env.PORT || 3000,
      dbPool = new Pool();


app.use(helmet());


// Routes for the web pages
app.get('/', function(req, res){
      // res.sendFile transfers the file at the given path.
      // The path.join() method joins all given path segments
      // together using the platform specific separator as a
      // delimiter, then normalizes the resulting path.
      res.sendFile(path.join(__dirname, 'views', 'index.html'));
});


// Routes for the JSON API
app.use('/api/v1' , apiVersion1);
app.use('/api/v2' , apiVersion2);

// Catch-all for any requests not already handled
app.use(function(req, res){
	return res.status(404).json({
            error: 'Invalid request',
      });
});


// Start the HTTP server
app.listen(port, function(){
	console.log("Listening on port " + port);
});
