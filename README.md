##Shallow Dive into Node.js##
Do you need to create dynamic web pages?  Do you need web services that can return JSON data?  What the heck is JSON anyway?!?
In this session we will cover the basics of setting up a Node.js webserver including modules, routing, web page templates, and JSON web services; everything you need to get up and running (ok, maybe just what you need to get up and walking).
