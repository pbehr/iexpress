Conn = function (idx, database) {
  this.db = require('/QOpenSys/QIBM/ProdData/OPS/Node6/os400/db2i/lib/db2a');
  this.conn = new this.db.dbconn();
  this.conn.conn(database);
  this.inuse = false;
  this.idx = idx;
  this.stmt = new this.db.dbstmt(this.conn);
}

Conn.prototype.free = function () {
  var newstmt = new this.db.dbstmt(this.conn);
  if (this.stmt) {
    delete this.stmt;
  }
  this.stmt = newstmt;
}

Conn.prototype.detach = function () {
  var newstmt = new this.db.dbstmt(this.conn);
  if (this.stmt) {
    delete this.stmt;
  }
  this.stmt = newstmt;
  this.inuse = false;
}

Conn.prototype.getInUse = function () {
  return this.inuse;
}

Conn.prototype.setInUse = function () {
  this.inuse = true;
}

Conn.prototype.getIndex = function () {
  return this.idx;
}

// Connection pool
// =============================================================================

Pool = function (opt) {
  opt = opt || {};
  this.pool = [];
  this.pmax = 0;
  this.pool_conn_incr_size = 8;
  this.database = opt.database || "*LOCAL";
}

Pool.prototype.attach = function (callback) {
  var valid_conn = false;
  while (!valid_conn) {
    // find available connection
    for (var i = 0; i < this.pmax; i++) {
      var inuse = this.pool[i].getInUse();
      if (!inuse) {
        this.pool[i].setInUse();
        callback(this.pool[i]);
        return;
      }
    }

    // expand the connection pool
    var j = this.pmax;
    for (var i = 0; i < this.pool_conn_incr_size; i++) {
      this.pool[j] = new Conn(j, this.database);
      j++;
    }
    this.pmax += this.pool_conn_incr_size;
  }
}

Pool.prototype.runStmt = function (sql, callback) {
  this.attach((conn) => {
    conn.stmt.exec(sql, function (result) {
      callback(result);
      conn.detach();
    });
  });
}

exports.Pool = Pool;